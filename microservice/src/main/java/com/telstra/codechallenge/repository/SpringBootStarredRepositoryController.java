package com.telstra.codechallenge.repository;

import java.time.LocalDate;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author Souvik Dey
 *
 */
@RestController
public class SpringBootStarredRepositoryController {

	private SpringBootStarredRepositoryService springBootStarredRepositoryService;
	public RestTemplate restTemplate;

	/**
	 * Constructor for SpringBootStarredRepositoryController
	 * @param springBootStarredRepositoryService
	 * @param restTemplate
	 */
	public SpringBootStarredRepositoryController(SpringBootStarredRepositoryService springBootStarredRepositoryService,
			RestTemplate restTemplate) {
		this.springBootStarredRepositoryService = springBootStarredRepositoryService;
		this.restTemplate = restTemplate;
	}

	/**
	 * returns a responseentity of StarredRepository type
	 * @param repositoryCount
	 * @return
	 */
	@GetMapping(value = "/hottestRepo/{count}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StarredRepository> getHottestepository(@PathVariable("count") String repositoryCount) {

		LocalDate currentDate = LocalDate.now();
		LocalDate searchDate = currentDate.minusWeeks(7l);
		StarredRepository starredRepository = null;
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Total-Count", repositoryCount);
		headers.set("Accept", "application/vnd.github.preview");
		MyPojo myPojo = springBootStarredRepositoryService.getPojo(repositoryCount, searchDate);
		for (int i = 0; i < myPojo.getItems().length; i++) {
			starredRepository = new StarredRepository(myPojo.getItems()[i].getHtml_url(),
					myPojo.getItems()[i].getWatchers_count(), myPojo.getItems()[i].getLanguage(),
					myPojo.getItems()[i].getDescription(), myPojo.getItems()[i].getName());
		}
		ResponseEntity<StarredRepository> response = new ResponseEntity<StarredRepository>(starredRepository, headers,
				HttpStatus.OK);
		return response;
	}

}
