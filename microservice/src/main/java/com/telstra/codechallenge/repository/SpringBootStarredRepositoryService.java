package com.telstra.codechallenge.repository;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author Souvik Dey
 *
 */
@Service
public class SpringBootStarredRepositoryService {

	@Value("${repo.base.url}")
	private String repoBaseUrl;

	@Value("${repo.trail.url}")
	private String repoTrailUrl;

	private RestTemplate restTemplate;

	/**
	 * Constructor for SpringBootStarredRepositoryService
	 * 
	 * @param restTemplate
	 */
	public SpringBootStarredRepositoryService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	/**
	 * Return a MyPojo class
	 * 
	 * @param repoCount
	 * @param searchDate
	 * @return MyPojo
	 */
	public MyPojo getPojo(String repoCount, LocalDate searchDate) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Total-Count", repoCount);
		headers.set("Accept", "application/vnd.github.preview");
		return restTemplate.getForObject(repoBaseUrl + searchDate + repoTrailUrl, MyPojo.class);
	}

}
