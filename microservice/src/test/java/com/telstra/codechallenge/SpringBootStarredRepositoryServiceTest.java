package com.telstra.codechallenge;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import com.telstra.codechallenge.repository.MyPojo;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SpringBootStarredRepositoryServiceTest {

	@LocalServerPort
	private int port;


	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testRepositories() throws RestClientException, MalformedURLException {
		ResponseEntity<MyPojo> response = restTemplate.getForEntity(
				new URL("https://api.github.com/search/repositories?q=created:>2019-07-15&sort=stars&order=desc")
						.toString(),
				MyPojo.class);
		assertEquals("200 OK", response.getStatusCode().toString());
	}

}
